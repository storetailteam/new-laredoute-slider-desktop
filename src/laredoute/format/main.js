"use strict";
try {

    const ctxt_pos = 0,
        ctxt_title = 1,
        ctxt_modal = 2,
        ctxt_prodlist = 2,
        ctxt_bg = 4,
        ctxt_logo = 5;

    var sto = window.__sto,
        settings = require("./../../settings.json"),
        $ = window.jQuery,
        html = require("./main.html"),
        placeholder = `${settings.format}_${settings.name}_${settings.creaid}`,
        style = require("./main.css"),
        container = $(html),
        creaid = settings.creaid,
        format = settings.format,
        products = {},
        nbProducts = 0,
        target = settings.cta_target != "_blank" && settings.target != "_self" ? "_self" : settings.target,
        container = $(html),
        helper_methods = sto.utils.retailerMethod,
        sto_global_products = [],
        sto_global_nbproducts = 0,
        pos, posGlobals;

        $('#csp_module_plp').hide();
        $('#trade_ccd_module_plp_1').hide();
        $('#trade_ccd_module_plp_2').hide();
        $('#trade_ccd_module_plp_3').hide();
        $('#csp_module_plp_search').hide();
        $('#trade_ccd_module_plp_search_1').hide();
        $('#trade_ccd_module_plp_search_2').hide();
        $('#trade_ccd_module_plp_search_3').hide();


    var fontRoboto = $('<link href="//fonts.googleapis.com/css?family=Roboto" rel="stylesheet">');
    $('head').first().append(fontRoboto);


    module.exports = {
        init: _init_()
    }

    function _init_() {

        sto.load([format, creaid], function (tracker) {

            var removers = {};
            removers[format] = {
                "creaid": creaid,
                "func": function () {
                    style.unuse();
                    container.remove(); //verifier containerFormat j'ai fait un copier coller
                }
            };

            if ($('.sto-format:not(.sto-AMSK, .sto-AFOO)').length == 3) {
                return removers;
            }

            /*if ($('div[data-pos="' + settings.btfpos + '"]').length > 0) {
              tracker.error({
                "tl": "placementOcuppied"
              });
              return removers;
              return false;
            }*/

            // if (settings.custom.length < 3) {
            //     tracker.error({
            //         "tl": "lessThanTwoProds"
            //     });
            //     return removers;
            //     return false;
            // }

            var settingsSort = {};

            Object.keys(settings).sort().forEach(function (key) {
                settingsSort[key] = settings[key];
            });

            $.each(settingsSort, function (name, value) {

                if (name.split('_')[1] === "img" && value != "" && name.split('_')[4] !== "size") {
                    if (products.hasOwnProperty('prod' + name.split('_')[2]) === false) {
                        products['prod' + name.split('_')[2]] = [];
                    }
                    products['prod' + name.split('_')[2]].push(value);
                }
                if (name.split('_')[1] === "txt" && value != "") {
                    if (products.hasOwnProperty('prod' + name.split('_')[2]) === true) {
                        products['prod' + name.split('_')[2]].push(value);
                    }
                }
                if (name.split('_')[1] === "url" && value != "") {
                    if (products.hasOwnProperty('prod' + name.split('_')[2]) === true) {
                        products['prod' + name.split('_')[2]].push(value);
                    }
                }
            })


            for (var i in products) {
                if (products.hasOwnProperty(i)) {
                    nbProducts++;
                }
            }


            // <editor-fold> CHECK FORM ***********************************************************
            if (nbProducts < 3) {
                tracker.error({
                    "tl": "lessThanThreeProds"
                });
                return removers;
                return false;
            }

            // if (products["prod01"].length < 3) {
            //     return removers;
            //     return false
            // }
            // </editor-fold> *********************************************************************


            try {


                var prodsInpage = $('#productList>li').length;


                if (prodsInpage >= 14 && prodsInpage <= 26 && $('.sto-' + format).length < 1) {

                } else if (prodsInpage > 26 && $('.sto-' + format).length < 3) {

                } else {
                    tracker.error({
                        "tl": "notEnoughProdsInPage"
                    });
                    return removers;
                    return false;
                }

                style.use();



                // <editor-fold> INSERT CONTAINER *****************************************************
                //insert format
                var insertFormat = function () {

                    var widthTile = $('#productList>li').width();
                    var widthLine = $('.product-list').width();
                    var nbTile;

                    if (widthTile * 2 > widthLine) {
                        nbTile = 1;
                    } else if (widthTile * 3 > widthLine) {
                        nbTile = 2;
                    } else {
                        nbTile = 3;
                    }

                    var sponso = $('.hl-beacon-universal').length;
                    if (sponso >= 1) {
                        sponso = 1;
                    }

                    var sponsoTech = $('.breakzone').length;
                    if (sponsoTech >= 1) {
                        sponsoTech = 1;
                    }


                    if (window.matchMedia("(max-width: 767px)").matches && nbTile == 1) {
                        if ($('.sto-format:not(.sto-AMSK, .sto-AFOO)').length == 0) {
                            $('#productList>li:visible').eq(nbTile * 5).before(container);
                            container.attr('data-pos', 6);
                            posGlobals = 6;
                        } else if ($('.sto-format:not(.sto-AMSK, .sto-AFOO)').length == 1) {
                            $('#productList>li:visible').eq(nbTile * 14).before(container);
                            container.attr('data-pos', 16);
                            posGlobals = 16;
                        } else if ($('.sto-format:not(.sto-AMSK, .sto-AFOO)').length == 2) {
                            $('#productList>li:visible').eq(nbTile * 23).before(container);
                            container.attr('data-pos', 26);
                            posGlobals = 26
                        }
                    } else if (window.matchMedia("(min-width: 768px)").matches && nbTile == 1) {
                        if ($('.sto-format:not(.sto-AMSK, .sto-AFOO)').length == 0) {
                            $('#productList>li:visible').eq(nbTile * (11 + sponsoTech)).before(container);
                            container.attr('data-pos', 12 + sponsoTech);
                            posGlobals = 12;
                        } else if ($('.sto-format:not(.sto-AMSK, .sto-AFOO)').length == 1) {
                            $('#productList>li:visible').eq(nbTile * (16 + sponsoTech)).before(container);
                            container.attr('data-pos', 18 + sponsoTech);
                            posGlobals = 18;
                        } else if ($('.sto-format:not(.sto-AMSK, .sto-AFOO)').length == 2) {
                            $('#productList>li:visible').eq(nbTile * (21 + sponsoTech)).before(container);
                            container.attr('data-pos', 24 + sponsoTech);
                            posGlobals = 24
                        }
                    } else {
                        if ($('.sto-format:not(.sto-AMSK, .sto-AFOO)').length == 0) {
                            $('#productList>li:visible').eq(nbTile * (5 + sponso)).before(container);
                            container.attr('data-pos', 6 + sponso);
                            posGlobals = 6;
                        } else if ($('.sto-format:not(.sto-AMSK, .sto-AFOO)').length == 1) {
                            $('#productList>li:visible').eq(nbTile * (14 + sponso)).before(container);
                            container.attr('data-pos', 16 + sponso);
                            posGlobals = 16;
                        } else if ($('.sto-format:not(.sto-AMSK, .sto-AFOO)').length == 2) {
                            $('#productList>li:visible').eq(nbTile * (23 + sponso)).before(container);
                            container.attr('data-pos', 26 + sponso);
                            posGlobals = 26
                        }
                    }
                }

                insertFormat();

                pos = $('.sto-' + placeholder + '-w-carrousel').attr('data-pos');


                // </editor-fold> *********************************************************************

                if ($('.sto-' + placeholder + '-w-carrousel').length === 1) {
                    tracker.display({
                        "po": pos
                    });
                }


                $('.sto-' + placeholder + '-w-carrousel').attr('data-type', 'redirection');
                // $('.sto-' + placeholder + '-w-carrousel').attr('data-pos', settings.btfpos);
                $('.sto-' + placeholder + '-w-carrousel').attr('data-format-type', settings.format);
                $('.sto-' + placeholder + '-w-carrousel').attr('data-crea-id', settings.creaid);


                var trackingView = false;
                var windowCenterTop = $(window).height() / 4;
                var windowCenterBot = $(window).height() - windowCenterTop;

                var eTop = $('.sto-' + placeholder + '-w-carrousel').offset().top;

                var dataPosition = $('.sto-' + placeholder + '-w-carrousel').attr('data-pos');

                var distanceToTop;
                var trackScroll = function () {
                    distanceToTop = eTop - $(window).scrollTop();
                    if (distanceToTop >= windowCenterTop && distanceToTop <= windowCenterBot && trackingView == false) {
                        tracker.view({
                            // "tl": dataPosition
                            "po": pos
                        });
                        trackingView = true;
                    }
                };
                trackScroll();


                $(window).scroll(function () {
                    trackScroll();
                });


                // <editor-fold> INSERT PRODUITS ******************************************************
                if (products["prod02"].length === 3) {
                    for (var e in products) {
                        if (products.hasOwnProperty(e)) {
                            var prodInd = e.substring(5);
                            var prodTxt = products[e][1];
                            var prodUrl = products[e][2];
                            $('.sto-' + placeholder + '-w-carrousel .sto-product-container').append('<li data-index="' + prodInd + '" class="product sto-vignette-klass"><div class="sto-' + placeholder + '-product-img"></div><div class="sto-' + placeholder + '-w-cta"><div class="sto-' + placeholder + '-product-claim"><span>' + prodTxt + '</span></div></div><div data-href="' + prodUrl + '" class="sto-' + placeholder + '-product-cta"></div></li>');
                        }
                    }
                } else if (products["prod02"].length === 1 && products["prod01"].length === 3) {
                    for (var e in products) {
                        if (products.hasOwnProperty(e)) {
                            var prodInd = e.substring(5);
                            $('.sto-' + placeholder + '-w-carrousel  .sto-product-container').append('<li data-index="' + prodInd + '" class="product sto-vignette-klass"><div class="sto-' + placeholder + '-product-img"></div></li>');
                        }
                    }
                    $('.sto-' + placeholder + '-w-carrousel').append('<div class="sto-' + placeholder + '-w-cta" data-href="' + products["prod01"][2] + '"><div class="sto-' + placeholder + '-product-claim"><span>' + products["prod01"][1] + '</span></div></div>');
                }


                // largeur carousel resize
                var carWidth = function () {
                    var nbProduits = nbProducts;
                    var widthProd = $('.sto-' + placeholder + '-w-carrousel .sto-product-container>.sto-vignette-klass').width();
                }

                carWidth();

                // </editor-fold> *********************************************************************



                // <editor-fold> WORDING **************************************************************
                $('<div class="sto-' + placeholder + '-wording"></div>').insertBefore('.sto-' + placeholder + '-w-slider');

                ;
                // if (settings.wording[0].length > 0) {
                if (Array.isArray(settings.wording) && settings.wording.length > 0) {
                    for (let i = 0; i < settings.wording.length; i++) {
                        let line;
                        if (i <= 2) {
                            if (settings.wording[i].length > 40) {
                                line = settings.wording[i].substring(0, 37) + "...";
                            } else {
                                line = settings.wording[i];
                            }
                            $('.sto-' + placeholder + '-wording').append('<span>' + line + '</span><br>');
                        }
                    }
                }

                // var firstLine = settings.wording[0].substring(0, 40);
                // var secondLine = settings.wording[1].substring(40);

                // if (settings.wording.length > 40) {
                //   if (secondLine.length > 40) {
                //     secondLine = secondLine.substring(0, 37) + "...";
                //      $('.sto-' + placeholder + '-wording').append('<span>' + firstLine + '</span><br><span>' + secondLine + '</span>');
                //   } else {
                //     $('.sto-' + placeholder + '-wording').append('<span>' + firstLine + '</span><br><span>' + secondLine + '</span>');
                //   }
                // } else {
                //   $('.sto-' + placeholder + '-wording').append('<span>' + firstLine + '</span>');
                // }

                // for (var i = 0; i < settings.wording.length; i++) {
                //
                //   var shortWording = settings.wording[i].substring(0, 40);
                //
                //   console.log(shortWording);
                //
                //   if (shortWording.length > 0) {
                //     $('.sto-' + placeholder + '-wording').append('<span>' + shortWording + '</span><br>');
                //   }
                // }

                // console.log(settings.wording);
                //
                // if (settings.wording != "") {
                //     $('<div class="sto-' + placeholder + '-wording"><span>' + settings.wording + "</span></div>").insertBefore('.sto-' + placeholder + '-w-slider');
                // }
                // </editor-fold> *********************************************************************


                // <editor-fold> CLICK REDIRECTION ****************************************************

                if ($('.sto-' + placeholder + '-w-carrousel>.sto-' + placeholder + '-w-cta').length > 0) {
                    $(document).on('click', '.sto-' + placeholder + '-w-carrousel>.sto-' + placeholder + '-w-cta', function () {
                        tracker.click({
                            "po": pos
                        });
                        window.open($(this).attr('data-href'), target);
                    });
                    $(document).on('click', '.sto-' + placeholder + '-w-slider .sto-product-container .product', function () {
                        tracker.click({
                            "po": pos
                        });
                        window.open($(this).closest('.sto-' + placeholder + '-w-carrousel').find('.sto-' + placeholder + '-w-cta').attr('data-href'), target);
                    });

                } else {
                    $(document).on('click', '.sto-' + placeholder + '-w-slider .sto-product-container .product', function () {
                        tracker.click({
                            "tl": 'prod' + (parseInt($(this).attr('data-index') - 1)) + '_' + $(this).find('.sto-' + placeholder + '-product-claim span').text().replace(/ /g, ''),
                            "po": pos
                        });
                        window.open($(this).find('.sto-' + placeholder + '-product-cta').attr('data-href'), target);
                    });
                }

                // </editor-fold> *********************************************************************


                // <editor-fold> RESIZE ***************************************************************
                var slidesQty = $('.sto-' + placeholder + '-w-carrousel .sto-product-container> .sto-vignette-klass').length,
                    pageW, slidesQty, sliderW;
                window.addEventListener("resize", function () {

                    var resizeFormat = function () {
                        var widthTile = $('#productList>li').width();
                        var widthLine = $('.product-list').width();
                        var nbTile;

                        if (widthTile * 2 > widthLine) {
                            nbTile = 1;
                        } else if (widthTile * 3 > widthLine) {
                            nbTile = 2;
                        } else {
                            nbTile = 3;
                        }

                        $('.sto-format').each(function () {
                            var dataPos = $(this).attr('data-pos');

                            // if (window.matchMedia("(max-width: 767px)").matches) {

                            if (nbTile == 1) {
                                if (window.matchMedia("(max-width: 767px)").matches) {
                                    if (dataPos >= 6 && dataPos <= 13) {
                                        $('#productList>li:visible').eq(nbTile * 5).before(this);
                                    } else if (dataPos >= 16 && dataPos <= 20) {
                                        $('#productList>li:visible').eq(nbTile * 14).before(this);
                                    } else if (dataPos >= 22 && dataPos <= 28) {
                                        $('#productList>li:visible').eq(nbTile * 23).before(this);
                                    }
                                }
                                if (window.matchMedia("(min-width: 768px)").matches) {
                                    if (dataPos >= 6 && dataPos <= 13) {
                                        $('#productList>li:visible').eq(nbTile * (dataPos - 1)).before(this);
                                    } else if (dataPos >= 16 && dataPos <= 20) {
                                        $('#productList>li:visible').eq(nbTile * (dataPos - 2)).before(this);
                                    } else if (dataPos >= 22 && dataPos <= 28) {
                                        $('#productList>li:visible').eq(nbTile * (dataPos - 3)).before(this);
                                    }
                                }
                            } else if (nbTile > 1) {
                                if (dataPos >= 6 && dataPos <= 13) {
                                    $('#productList>li:visible').eq(nbTile * (dataPos - 1)).before(this);
                                } else if (dataPos >= 16 && dataPos <= 17) {
                                    $('#productList>li:visible').eq(nbTile * (dataPos - 2)).before(this);
                                } else if (dataPos >= 23 && dataPos <= 27) {
                                    $('#productList>li:visible').eq(nbTile * (dataPos - 3)).before(this);
                                }
                            }
                        });
                    }
                    resizeFormat();


                    sliderW = 0;
                    for (var j = 0; j < slidesQty; j++) {
                        sliderW = sliderW + $('.sto-' + placeholder + '-w-carrousel .sto-product-container> .sto-vignette-klass').eq(j).outerWidth(true, true);
                    }

                    $('.sto-' + placeholder + '-w-carrousel .sto-product-container').outerWidth(sliderW + 2);

                    //Format height homotetique
                    $('.sto-' + placeholder + '-w-carrousel').css('height', ((65 * $('.sto-' + placeholder + '-w-carrousel').width()) / 100) + 'px');

                    $('.sto-' + placeholder + '-w-carrousel .product').css('width', ((40 * $('.sto-' + placeholder + '-w-carrousel').width()) / 100) + 'px');
                    $('.sto-' + placeholder + '-product-img').css('height', $('.sto-' + placeholder + '-product-img').width());
                    //Logo height homotetique
                    $('.sto-' + placeholder + '-logo').css('width', ((33 * $('.sto-' + placeholder + '-w-carrousel').width()) / 100) + 'px');
                    // $('.sto-' + placeholder + '-logo').css('height', ((75 * $('.sto-' + placeholder + '-logo').width()) / 100) + 'px');

                    $('.sto-' + placeholder + '-w-carrousel .sto-product-container').css('width', $('.sto-' + placeholder + '-w-carrousel .product').outerWidth(true) * slidesQty + 'px');


                    // displostion fleches
                    var heightImg = $('.sto-' + placeholder + '-product-img').css('height');
                    $('.sto-' + placeholder + '-slider-left, .sto-' + placeholder + '-slider-right').css('height', heightImg);


                    // slider left margin
                    if (window.matchMedia("(max-width: 767px)").matches) {
                        $('.sto-' + placeholder + '-slider.sto-product-container').css({
                            'left': '15px',
                            'transform': 'translateX(0%)'
                        });
                    } else {
                        if ($('.sto-' + placeholder + '-slider').width() <= $('.sto-' + placeholder + '-w-slider').width()) {
                            $('.sto-' + placeholder + '-slider-right').fadeOut(1);
                            $('.sto-' + placeholder + '-slider').css({
                                'left': 'calc(50% + 8px)',
                                'transform': 'translateX(-50%)'
                            })
                        } else {
                            $('.sto-' + placeholder + '-slider-right').fadeIn(1);
                            $('.sto-' + placeholder + '-slider').css({
                                'left': '15px',
                                'transform': 'translateX(0%)'
                            })
                        }
                    }

                    // $('.sto-' + placeholder + '-w-carrousel .sto-product-container').animate({
                    //   'left': 15
                    // }, 0);


                });
                window.setTimeout(function () {
                    //window.dispatchEvent(new window.Event("resize"));
                    var evt = window.document.createEvent('UIEvents');
                    evt.initUIEvent('resize', true, false, window, 0);
                    window.dispatchEvent(evt);
                });


                // STORE FORMAT FOR FILTERS
                sto.globals["print_creatives"].push({
                    html: container,
                    parent_container_id: "#productList",
                    type: settings.format,
                    crea_id: settings.creaid,
                    position: posGlobals
                });

            } catch (e) {
                console.log(e);
                style.unuse();
                $(".sto-" + placeholder + "-container").remove();
                tracker.error({
                    // "te": "onBuild-format",
                    "tl": e
                });
            }

            // </editor-fold> *********************************************************************



            // <editor-fold> CARROUSEL ARROWS *****************************************************
            if ($('.sto-' + placeholder + '-slider').width() <= $('.sto-' + placeholder + '-w-slider').width()) {
                $('.sto-' + placeholder + '-slider-right').fadeOut(1);
                $('.sto-' + placeholder + '-slider').css({
                    'left': 'calc(50% + 8px)',
                    'transform': 'translateX(-50%)'
                })
            }

            var slideDist = 295;
            $('.sto-' + placeholder + '-slider-left').fadeOut(1);


            // left
            $('.sto-' + placeholder + '-slider-left').on('click', function () {
                $('.sto-' + placeholder + '-slider-right').fadeIn(300);
                var slideWidth = $('.sto-' + placeholder + '-w-carrousel .sto-product-container').width();
                var positionInit = $('.sto-' + placeholder + '-slider.sto-product-container').position().left;
                if ((positionInit * -1) < slideDist && positionInit < 50) {
                    $('.sto-' + placeholder + '-slider.sto-product-container').animate({
                        left: '15'
                    }, 300, 'swing');
                    $('.sto-' + placeholder + '-slider-left').fadeOut(300);
                } else if (positionInit < 50) {
                    var positionLeft = $('.sto-' + placeholder + '-slider.sto-product-container').position().left + slideDist;
                    $('.sto-' + placeholder + '-slider.sto-product-container').animate({
                        left: positionLeft + 'px'
                    }, 300, 'swing');
                }
                tracker.browse({
                    "tl": "swipeLeft",
                    "po": pos
                });
            });

            // right
            $('.sto-' + placeholder + '-slider-right').on('click', function () {
                $('.sto-' + placeholder + '-slider-left').fadeIn(300);
                var sliderContainerWidth = $('.sto-' + placeholder + '-w-slider').width();
                var slideWidth = $('.sto-' + placeholder + '-w-carrousel .sto-product-container').width();
                var positionInit = $('.sto-' + placeholder + '-slider.sto-product-container').position().left;
                var positionRight = slideWidth - (sliderContainerWidth - positionInit);
                if (positionRight > slideDist) {
                    var positionLeft = $('.sto-' + placeholder + '-slider.sto-product-container').position().left - slideDist;
                    $('.sto-' + placeholder + '-slider.sto-product-container').animate({
                        left: positionLeft + 'px'
                    }, 300, 'swing');
                } else if (positionRight <= slideDist) {
                    $('.sto-' + placeholder + '-slider.sto-product-container').animate({
                        left: (sliderContainerWidth - slideWidth) + 'px'
                    }, 300, 'swing');
                    $('.sto-' + placeholder + '-slider-right').fadeOut(300);
                }
                tracker.browse({
                    "tl": "swipeRight",
                    "po": pos
                });
            });


            // tracking swipe
            var lastPos = 0;
            $('.sto-' + placeholder + '-w-slider').on('scroll', function () {
                var currPos = $('.sto-' + placeholder + '-w-slider').scrollLeft();
                clearTimeout($.data(this, 'scrollTimer'));
                $.data(this, 'scrollTimer', setTimeout(function () {

                    if (currPos > lastPos) {
                        tracker.browse({
                            "tl": "swipeRight",
                            "po": pos
                        });
                    }
                    if (currPos < lastPos) {
                        tracker.browse({
                            "tl": "swipeLeft",
                            "po": pos
                        });
                    }
                    lastPos = currPos;
                }, 250));
            });

            // </editor-fold> *********************************************************************


            var heightFormat = function () {

                var products = $(".sto-product-container>li.product");
                var sliderContainer = $('.sto-BA_slider_-w-slider');
                var heightFormat = 0;
                products.each(function () {
                    var thisHeight = $(this).height();
                    if (thisHeight > heightFormat) {
                        heightFormat = thisHeight;
                    }
                });
                sliderContainer.css("height", heightFormat + 50);
                products.css("height", heightFormat + 30);
            }
            //
            // setTimeout(function() {
            //     heightFormat();
            // }, 500);




            return removers;
        });

    }


} catch (e) {
    console.log(e);
}
